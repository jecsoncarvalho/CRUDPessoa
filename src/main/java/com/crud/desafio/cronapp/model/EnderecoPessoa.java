package com.crud.desafio.cronapp.model;

import javax.persistence.Embeddable;

@Embeddable
public class EnderecoPessoa {
    private String pais;
    private String cidade;
    
    public EnderecoPessoa(){}
    
    public EnderecoPessoa(String pais, String cidade){
    	this.pais = pais;
    	this.cidade = cidade;
    }
    
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
}
