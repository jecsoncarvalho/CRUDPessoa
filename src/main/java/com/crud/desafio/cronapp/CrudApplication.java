package com.crud.desafio.cronapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.crud.desafio.cronapp.model.EnderecoPessoa;
import com.crud.desafio.cronapp.model.Pessoa;
import com.crud.desafio.cronapp.repository.PessoaRepository;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class CrudApplication {

	@Autowired
	PessoaRepository pessoaRepository;
	public static void main(String[] args) {
		SpringApplication.run(CrudApplication.class, args);
	}
}
