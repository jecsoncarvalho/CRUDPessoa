var app = angular.module('crudApp', []);

app.controller('RESTController', function($scope, $http) {

    var getAllUrl = "inicio/listarTodos";

    $scope.updatedElement = {};
    $scope.viewElement = {};

    $scope.getAllElements = function(){

        $http.get(getAllUrl).then(function (response) {
            $scope.allElements = (response.data);
        }, function error(response) {
            $scope.postResultMessage = "Erro com status: " +  response.statusText;
        });
    }

    $scope.deleteElement = function (element,index) {
        var deleteElementUrl = "inicio/deletar/"+element.id;
        $http.delete(deleteElementUrl).then(function () {
            $scope.allElements.splice(index,1);
        });
    }
    
    $scope.add = function () {
        var pessoaData = {
            primeiroNome : $scope.primeiroNomeForm,
            sobrenome : $scope.sobrenomeForm,
            idade : $scope.idadeForm,
            email : $scope.emailForm,
            enderecoPessoa : {
                pais : $scope.paisForm,
                cidade : $scope.cidadeForm,
            }
        };

        var url = "inicio/adicionar";

        $http.post(url,pessoaData).then(function (success) {
            console.log(success.data);
            $('#addModal').modal('hide');
            $scope.allElements.push(success.data);
            $scope.primeiroNomeForm='';
            $scope.sobrenomeForm='';
            $scope.idadeForm='';
            $scope.emailForm = '';
            $scope.paisForm='';
            $scope.cidadeForm='';
        });

    }

    $scope.dataTransfer = function (element) {
        $scope.updatedElement.id = element.id;
        $scope.updatedElement.primeiroNome = element.primeiroNome ;
        $scope.updatedElement.sobrenome = element.sobrenome ;
        $scope.updatedElement.idade = element.idade;
        $scope.updatedElement.email = element.email;
        $scope.updatedElement.pais = element.enderecoPessoa.pais;
        $scope.updatedElement.cidade = element.enderecoPessoa.cidade;
    }


    $scope.openUpdate = function (index) {

        var updated = {
        	primeiroNome : $scope.updatedElement.primeiroNome,
        	sobrenome : $scope.updatedElement.sobrenome,
        	idade : $scope.updatedElement.idade,
            email : $scope.updatedElement.email,
            enderecoPessoa : {
                pais : $scope.updatedElement.pais,
                cidade : $scope.updatedElement.cidade,
            }
        };

        var url = "inicio/atualizar/"+$scope.updatedElement.id;

        $http.put(url,updated).then(function (success) {
            console.log(success);
            $('#updateModal').modal('hide');
            //noinspection JSAnnotator,JSAnnotator
            $scope.allElements = $scope.getAllElements();
            $scope.updatedElement.primeiroNome = "";
            $scope.updatedElement.sobrenome = "";
            $scope.updatedElement.idade = "";
            $scope.updatedElement.email = "";
            $scope.updatedElement.pais = "";
            $scope.updatedElement.cidade = "";
        });
    }


});